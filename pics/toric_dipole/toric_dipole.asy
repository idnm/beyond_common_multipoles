settings.outformat = "png";
settings.render=1;
real scale=400pt;
size(scale,0);

import graph3;
currentprojection = perspective(100*dir(45,30));

real R=5, r=2;
real an1=90, an2=270;
real lw=scale/50;
real arrowsize=lw*6;

// torus cross section
path3 crossSection = Circle(r=r, c=(R,0,0), normal=Y);

// torus as rotation surface
surface torus = surface(crossSection, c=(0,0,0), axis=Z);
draw(torus, red+0.5white);

// current lines
path3 currentLineUp = reverse(arc(c=R*Y, r=r, phi1=an1, phi2=an2, theta1=90, theta2=90, normal=X));
path3 currentLineDown = reverse(arc(c=R*Y, r=-r, phi1=an1, phi2=an2, theta1=90, theta2=90, normal=X));

// four current loops along the torus
for(int an=0; an < 370; an=an+180) {
draw(rotate(an,Z)*currentLineUp, red+linewidth(lw), Arrow3(size=arrowsize, Relative(0.75), emissive(red)));
draw(rotate(an,Z)*currentLineDown, red+linewidth(lw));//, Arrow3(size=arrowsize, Relative(0.5)));
}