settings.outformat="pdf";
settings.render=8.46;
import three;
size(8cm, 0);

currentprojection = orthographic(5,2,3);

material m = opacity(0.65)+palegreen;

surface s1 = scale(5,0.4,1)*unitcube;
surface s2 = shift(5,0,0)*scale(3,3,3)*unitcube;

draw(s1, surfacepen=m);
draw(s2, surfacepen=m);