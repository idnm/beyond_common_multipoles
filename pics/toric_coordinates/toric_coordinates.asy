settings.outformat = "png";
settings.render=1;
real sc = 100pt;

import graph3;
currentprojection = perspective(dir(45,30));

real R=4sc, r=2sc;
//real R2=4.6, r2=2sqrt(R2^2-R1^2+r1^2/4);
//real dr=R1/10;
real an1=90, an2=360;
//real lw=10pt;
//real arrowsize=60pt;

path3 crossSection = Circle(r=r, c=(R,0,0), normal=Y);

surface torus = surface(crossSection, c=(0,0,0), axis=Z);

draw(torus, blue+opacity(0.7));

//path3 current1Line1Up = arc(c=R1*Y, r=r1, phi1=an1, phi2=an2, theta1=90, theta2=90, normal=X);
//interaction constantsize = settings.autobillboard ? interaction(1,true) : interaction(0,true);


Label xlabel = Label("$x$", position=EndPoint);
Label ylabel = Label("$y$", position=EndPoint);
Label zlabel = Label("$z$", position=EndPoint);

triple xx = 10sc*X;
triple yy = 10sc*Y;
triple zz = 10sc*Z;

draw(O--xx, Arrow3());
draw(O--yy, Arrow3());
draw(O--zz, Arrow3());

//	label(position=xx, L=xlabel, constantsize);