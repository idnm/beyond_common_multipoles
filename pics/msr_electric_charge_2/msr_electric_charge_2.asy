// FIGURE SETUP
settings.outformat = "pdf";
settings.render=1;

import graph3;
currentprojection = perspective(100*dir(60,50));

real scale = 400pt;
size(scale, 0);

include "../setup_and_functions.asy"; // This needs to be imported after the scale is defined

// CONTENT
draw_charge_msr(c=O, r=0.8, charge=1, otherpen=blue+opacity(1));
draw_semisphere(c=O, r=1.5, charge=1);

draw_section(u=3Y,v=3Z, c=-1.5Y-1.5Z, axis=false);

// LABELS
//label("$q$",(0.01,-0.3,-0.3), red+fontsize(100pt));
//label("$q_1$",(0.01,-0.85,-0.85), blue+fontsize(100pt));
//label("$q_2$",(0.01,-1.3,-1.3), red+fontsize(30pt));