// FIGURE SETUP
settings.outformat = "pdf";
settings.render=1;

import graph3;
currentprojection = perspective(100*dir(60,50));

real scale = 400pt;
size(scale, 0);

include "../setup_and_functions.asy"; // This needs to be imported after the scale is defined

// CONTENT
draw_charge_msr(c=O, r=1, charge=1);
draw_section(u=2Y,v=2Z,c=-Y-Z, axis=false);

// Labels
//label("$+q$",(0,-0.25,-0.25), red+fontsize(500pt));
//label("$-q$",(0,-0.9,-0.9), blue+fontsize(500pt));