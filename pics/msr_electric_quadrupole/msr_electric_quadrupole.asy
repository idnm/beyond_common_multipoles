// FIGURE SETUP
settings.outformat = "png";
settings.render=1;

import graph3;
currentprojection = perspective(100*dir(60,50));

real scale = 600pt;
size(scale, 0);

include "../setup_and_functions.asy"; // This needs to be imported after the scale is defined

// CONTENT
draw_charge_msr(c=-1.5Y-1.2Z, r=1, charge=1);
draw_charge_msr(c=1.5Y-1.2Z, r=1, charge=-1);
draw_charge_msr(c=-1.5Y+1.2Z, r=1, charge=-1);
draw_charge_msr(c=1.5Y+1.2Z, r=1, charge=1);

draw_section(u=5Y,v=4.4Z, c=-2.5Y-2.2Z, axis=false);
