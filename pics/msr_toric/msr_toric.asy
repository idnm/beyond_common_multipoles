settings.outformat = "png";
settings.render=2;
real scale=500pt;
size(scale,0);

import graph3;
currentprojection = perspective(100*dir(45,30));

real R1=5, r1=4;
real R2=4.6, r2=3sqrt(R2^2-R1^2+r1^2/4); // formula is not correct!
real dr=R1/10;
real an1=90, an2=270;
real lw=scale/100;
real arrowsize=lw*7;

pen Dashed = linetype(new real[] {4,4});
defaultpen(fontsize(scale/2));
pen bc = blue+cyan;
pen rc = red;//+0.25magenta;

//Cross-sections for two tori
path3 crossSection1 = Circle(r=r1, c=(R1,0,0), normal=Y);
path3 crossSection2 = Circle(r=r2, c=(R2,0,0), normal=Y);

//Half-tori as rotation surface
surface torus1 = surface(crossSection1, c=(0,0,0), axis=Z, angle1=an1, angle2=an2);
surface torus2 = surface(crossSection2, c=(0,0,0), axis=Z, angle1=an1, angle2=an2);

draw(torus1, blue+0.2white+opacity(0.7));
draw(torus2, red+0.2white+opacity(0.5));

//Currents at right end
draw(arc(c=R1*Y, r=r1, phi1=an1, phi2=an2+180, theta1=90, theta2=90, normal=X),bc+linewidth(lw), Arrow3(size=arrowsize, emissive(bc), position=Relative(0.25)));
draw(reverse(arc(c=R2*Y, r=r2, phi1=an1, phi2=an2+180, theta1=90, theta2=90, normal=X)),rc+linewidth(lw), Arrow3(size=0.75arrowsize, emissive(rc), position=Relative(0.85)));

//Currents at left end
draw(reverse(arc(c=-R1*Y, r=r1, phi1=an1, phi2=an2+180, theta1=90, theta2=90, normal=X)),bc+linewidth(lw), Arrow3(size=arrowsize, emissive(bc), position=Relative(0.75)));
draw(arc(c=-R2*Y, r=r2, phi1=an1, phi2=an2+180, theta1=90, theta2=90, normal=X),rc+linewidth(lw), Arrow3(size=0.75arrowsize, emissive(rc), position=Relative(0.3)));

// Current Labels
//label("$j$",(r1/20,R1+0.8r1,0), bc);
//label("$j$",(r1/20,R2+1.4r2,0), rc);

// Tori centers
path3 mainCircle1 = arc(c=O, r=R1, phi1=an1, phi2=an2+170, theta1=90, theta2=90, normal=Z);
path3 mainCircle2 = arc(c=O, r=R2, phi1=an1, phi2=an2+170, theta1=90, theta2=90, normal=Z);

draw(mainCircle1,blue+Dashed+linewidth(lw/2));
draw(mainCircle2,red+Dashed+linewidth(lw/2));

//Symmetry plane
path3 symmetryPlane = plane((0,4R1,0),(0,0,3R1),(0,-2R1,-1.5R1));
path3 symmetryAxis = (0,0,-1.6R1) -- (0,0,1.6R1);
draw(symmetryPlane,yellow+linewidth(lw/2));
draw(surface(symmetryPlane),yellow+opacity(0.1),light=nolight); 
draw(symmetryAxis,yellow+linewidth(lw/2)+Dashed);