settings.outformat = "png";
settings.render=1;
size(1000pt, 0);

import graph3;
currentprojection = perspective(100*dir(45,30));

real R1=1; // circle radius
real an1=90, an2=270; // section plane angles
real arrowsize=100pt; // arrow size
real lw=10pt; // linewidth

// current template
path3 currentLine(real r, real z) {return arc(c=O+z*Z, r=r, phi1=an2, phi2=an1, theta1=90, theta2=90, normal=Z);} 

// current circle
draw(currentLine(R1, 0), red+linewidth(1.5lw), Arrow3(Relative(1), size=arrowsize, emissive(red)));
draw(rotate(180, Z)*currentLine(R1, 0), red+linewidth(1.5lw), Arrow3(Relative(1), size=arrowsize, emissive(red)));