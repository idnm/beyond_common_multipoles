// LINE STYLES
real lw=scale/50;
real arrowsize=6lw;
real an1=90, an2=270;

pen Dashed = linetype(new real[] {4,4}); // Fine dash

// COLORS
real op = 1;//0.7;
pen[] charge_pen(real charge) {
	// Returns {point charge pen, line pen, surface pen}
	if (charge==1) {pen[] arr={red, 0.5red+linewidth(lw), red+opacity(op)}; return arr;}
	if (charge==-1) {pen[] arr={blue, 0.5blue+linewidth(lw), blue+opacity(op)}; return arr;}
	pen[] arr={black, black, black}; return arr;
	}

// SPHERIC TEMPLATES

// Semispheric shell
void draw_semisphere(triple c=O, real r=1, real charge=1, pen otherpen=white) 
	{
	pen p_line = charge_pen(charge)[1]; pen p_surf = charge_pen(charge)[2];
	if (otherpen != white) {p_surf = otherpen;} // Sometimes you have to cheat
	// Half-sphere as a rotation surface from meridian
	path3 meridian = arc(c=c, r=r, theta1=180, theta2=0, phi1=0, phi2=0, normal=Y);
	draw(surface(meridian, c=c, axis=Z, angle1=an1, angle2=an2, n=36), p_surf);
	// Emphasize section circle
	draw(circle(c=c,r=r,normal=X), p_line);
	}

// Semisperic shell with central charge
void draw_charge_msr(triple c=O, real r=1, real charge=1, pen otherpen=white)
	{
	pen cp = charge_pen(charge)[0]; 
	material sp = material(diffusepen=cp, emissivepen=0.5cp, specularpen=cp); // Otherwise point charges are too dark.
	
	draw(shift(c)*scale3(0.05r)*unitsphere, surfacepen=sp);
	draw_semisphere(c=c, r=r, charge=-charge, otherpen=otherpen);
	}

// Section plane
void draw_section(triple u, triple v, triple c, bool axis=true) { 
	real p=1.2; // renormalization coefficient
	u = p*u; v=p*v; c=p*c;
	path3 symmetryPlane = plane(u,v,c);
	draw(symmetryPlane,yellow+linewidth(lw/2));
	draw(surface(symmetryPlane),yellow+opacity(0.1),light=nolight); 
	if (axis) {
		path3 symmetryAxis = c+0.5u -- c+0.5u+v;
		draw(scale3(1.1)*symmetryAxis,yellow+linewidth(lw/2)+Dashed);
		}
	}
