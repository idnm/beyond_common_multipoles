settings.outformat = "png";
settings.render=2;
real scale = 500pt;
size(scale, 0);

import graph3;
currentprojection = perspective(100*dir(45,30));

real R1=5, r1=4; // torus radii
real R2=sqrt(R1^2-r1^2/2); // inner circle radius
real an1=90, an2=270; // section plane angles
real arrowsize=scale/10; // arrow size
real op=0.7; // opacity
real lw=scale/100; // linewidth

pen Dashed = linetype(new real[] {4,4});
defaultpen(fontsize(scale/10));

// half-torus
path3 crossSection(real r, real R) {return Circle(r=r, c=(R,0,0), normal=Y);}
draw(surface(crossSection(r1, R1), c=O, axis=Z, angle1=an1, angle2=an2),blue+0.2white+opacity(op));

// current template
path3 currentLine(real r, real z) {return arc(c=O+z*Z, r=r, phi1=an1, phi2=an2, theta1=90, theta2=90, normal=Z);} 

// torus currents
draw(currentLine(R1+r1, 0.6r1), blue+cyan+linewidth(lw), Arrow3(size=arrowsize, emissive(blue)));
//draw(currentLine(R1-r1, 0), blue+cyan+linewidth(lw), Arrow3(Relative(0.5),size=arrowsize));

// torus geometric center
draw(currentLine(R1, 0), blue+linewidth(lw/2)+Dashed);
draw(rotate(180, Z)*currentLine(R1, 0), blue+linewidth(lw/2)+Dashed);

// opposite inner current
draw(reverse(currentLine(R2, 0)), red+linewidth(1.5lw), Arrow3(Relative(1), emissive(red), size=arrowsize));
//draw(rotate(180, Z)*reverse(currentLine(R2, 0)), red+linewidth(1.5lw), Arrow3(Relative(0.5), size=arrowsize));

// emphasize intersection with the symmetry plane
draw(circle(r=r1, c=(0,R1,0), normal=X),blue+linewidth(lw/2));
draw(circle(r=r1, c=(0,-R1,0), normal=X),blue+linewidth(lw/2));

draw(surface(circle(r1/20, c=(r1/20,R1+r1,0.6r1), normal=X)), darkblue, light=nolight);
draw(surface(circle(r1/20, c=(r1/20,-R2,0), normal=X)), darkred, light=nolight);

// Current Labels
//label("$j$",(r1/20,-R1-r1,r1), blue);
//label("$j$",(r1/20,R2-r1/4,0), red);

// symmetry plane
path3 symmetryPlane = plane((0,4.2R1,0),(0,0,3R1),(0,-2.1R1,-1.5R1));
path3 symmetryAxis = (0,0,-1.6R1) -- (0,0,1.6R1);
draw(symmetryPlane,yellow+linewidth(lw/2));
draw(surface(symmetryPlane),yellow+opacity(0.1),light=nolight); 
draw(symmetryAxis,yellow+linewidth(lw/2)+Dashed);